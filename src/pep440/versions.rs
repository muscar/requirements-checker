use std::fmt;

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum VersionIdSegmentRelease {
    Dev,
    Alpha,
    Beta,
    Rc,
    Point,
    Post,
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct VersionIdSegment {
    pub release: VersionIdSegmentRelease,
    pub value: i64,
}

impl fmt::Display for VersionIdSegment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let release = match self.release {
            VersionIdSegmentRelease::Dev => "dev",
            VersionIdSegmentRelease::Alpha => "a",
            VersionIdSegmentRelease::Beta => "b",
            VersionIdSegmentRelease::Rc => "rc",
            VersionIdSegmentRelease::Point => "",
            VersionIdSegmentRelease::Post => "post",
        };
        write!(f, "{}{}", release, self.value)
    }
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct VersionId(Vec<VersionIdSegment>);

impl fmt::Display for VersionId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{}",
               self.0.iter().map(|s| s.to_string()).collect::<Vec<String>>().join("."))
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum ComparisonOperator {
    Compatible,
    Match,
    Exclude,
    ArbitraryEq,
    Lt,
    Gt,
    Lte,
    Gte,
}

#[derive(Debug)]
pub struct VersionClause {
    pub operator: ComparisonOperator,
    pub number: VersionId,
}

#[derive(Debug)]
pub struct VersionSpecifier {
    pub clauses: Vec<VersionClause>,
}

#[derive(Debug)]
pub struct RequirementSpecifier {
    pub name: String,
    pub version: VersionSpecifier,
}

impl VersionId {
    pub fn new(segments: Vec<VersionIdSegment>) -> Self {
        VersionId(segments)
    }
}
