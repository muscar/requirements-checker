use std::iter::Peekable;
use std::str::Chars;

use pep440::versions::*;

pub struct TextReader<'a> {
    iter: Peekable<Chars<'a>>,
}

impl<'a> TextReader<'a> {
    pub fn new(text: &'a str) -> Self {
        TextReader { iter: text.chars().peekable() }
    }

    fn peek(&mut self) -> Option<char> {
        self.iter.peek().cloned()
    }

    fn read(&mut self) -> Option<char> {
        self.iter.next()
    }

    fn read_while<Pred: Fn(char) -> bool>(&mut self, p: Pred) -> String {
        let mut acc = String::with_capacity(32);
        while let Some(c) = self.peek() {
            if !p(c) {
                break;
            }
            acc.push(self.read().unwrap())
        }
        acc
    }
}

pub fn parse_requirement_specifier(s: String) -> RequirementSpecifier {
    let mut reader = TextReader::new(&s);
    RequirementSpecifier {
        name: parse_ident(&mut reader),
        version: parse_version_specifier(&mut reader),
    }
}

fn parse_version_specifier(reader: &mut TextReader) -> VersionSpecifier {
    let mut clauses = Vec::new();
    clauses.push(parse_version_clause(reader));
    VersionSpecifier { clauses: clauses }
}

fn parse_version_clause(reader: &mut TextReader) -> VersionClause {
    VersionClause {
        operator: parse_comparison_operator(reader),
        number: parse_version_id(reader),
    }
}

fn parse_comparison_operator(reader: &mut TextReader) -> ComparisonOperator {
    // XXX This doesn't support ===
    reader.read_while(|c| c.is_whitespace());
    match reader.peek() {
        Some('~') | Some('!') | Some('=') => {
            let c = reader.read().unwrap();
            if let Some('=') = reader.read() {
                match c {
                    '~' => ComparisonOperator::Compatible,
                    '!' => ComparisonOperator::Exclude,
                    '=' => ComparisonOperator::Match,
                    _ => panic!("can't happen"),
                }
            } else {
                panic!("parse error: expecting '=' after {}", c)
            }
        }
        Some('<') => {
            reader.read();
            if let Some('=') = reader.peek() {
                reader.read();
                return ComparisonOperator::Lte;
            }
            ComparisonOperator::Lt
        }
        Some('>') => {
            reader.read();
            if let Some('=') = reader.peek() {
                reader.read();
                return ComparisonOperator::Gte;
            }
            ComparisonOperator::Gt
        }
        x => panic!("parse error: invalid comparison operator (c={:?})", x),
    }
}

pub fn parse_version_id(reader: &mut TextReader) -> VersionId {
    reader.read_while(|c| c.is_whitespace());
    let mut segments = Vec::new();
    segments.push(parse_version_id_segment(reader));
    while let Some('.') = reader.peek() {
        reader.read();
        segments.push(parse_version_id_segment(reader));
    }
    VersionId::new(segments)
}

fn parse_version_id_segment(reader: &mut TextReader) -> VersionIdSegment {
    let c = reader.peek().unwrap();
    if c.is_digit(10) {
        return VersionIdSegment {
            release: VersionIdSegmentRelease::Point,
            value: parse_number(reader),
        };
    }
    let release = match reader.read_while(|c| c.is_alphabetic()).as_ref() {
        "a" => VersionIdSegmentRelease::Alpha,
        "b" => VersionIdSegmentRelease::Beta,
        "rc" => VersionIdSegmentRelease::Rc,
        "post" => VersionIdSegmentRelease::Post,
        "dev" => VersionIdSegmentRelease::Dev,
        release => {
            panic!("parse error: unkonwn version ID segment release: {}",
                   release)
        }
    };
    VersionIdSegment {
        release: release,
        value: parse_number(reader),
    }
}

fn parse_ident(reader: &mut TextReader) -> String {
    reader.read_while(|c| c.is_alphanumeric() || c == '-' || c == '_')
}

fn parse_number(reader: &mut TextReader) -> i64 {
    reader.read_while(|c| c.is_digit(10))
        .parse::<i64>()
        .expect("parse error: expecting number in version")
}
