use std::io::Read;

use hyper::client::Client;
use hyper::header::Headers;

use rustc_serialize::json::Json;

use pep440::versions::VersionId;
use pep440::parser::{TextReader, parse_version_id};

pub struct DevpiClient {
    url: String,
    auth_token: String,
}

impl DevpiClient {
    pub fn new(url: String, auth_token: String) -> Self {
        DevpiClient {
            url: url,
            auth_token: auth_token,
        }
    }

    pub fn get_versions(&self, name: &str) -> Vec<VersionId> {
        let url = format!("{}/{}/", self.url, name);
        let client = Client::new();
        let mut headers = Headers::new();
        headers.set_raw("Accept", vec![b"application/json".to_vec()]);
        headers.set_raw("Authorization",
                        vec![format!("Basic {}", self.auth_token).bytes().collect()]);
        let mut resp = client.get(&url).headers(headers).send().unwrap();
        assert!(resp.status.is_success());
        let mut raw_json = String::new();
        resp.read_to_string(&mut raw_json).expect("couldn't read devpi JSON");
        let json = Json::from_str(&raw_json).unwrap();
        let obj = json.as_object().unwrap();
        let result = obj.get("result").unwrap();
        result.as_object()
            .unwrap()
            .keys()
            .cloned()
            .map(|s| parse_version_id(&mut TextReader::new(&s)))
            .collect()
    }
}
