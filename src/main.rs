#![feature(proc_macro)]
#![feature(plugin)]

extern crate hyper;
#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
extern crate rustc_serialize;

mod devpi;
mod pep440;

use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};

use pep440::parser;
use pep440::versions::{ComparisonOperator, RequirementSpecifier, VersionId};

#[derive(Serialize, Deserialize)]
struct Config {
    url: String,
    auth_token: String,
}

fn check_version<'a>(requirement: &'a RequirementSpecifier,
                     devpi_versions: &'a [VersionId])
                     -> Option<(&'a VersionId, &'a VersionId)> {
    let clause = &requirement.version.clauses[0];
    let local_version = &clause.number;
    let latest_devpi_version = devpi_versions.iter().max().unwrap();
    if clause.operator == ComparisonOperator::Match && local_version < latest_devpi_version {
        return Some((local_version, latest_devpi_version));
    }
    None
}

fn load_config(path: &str) -> Config {
    let mut f = File::open(path).unwrap();
    let mut raw_config = String::new();
    f.read_to_string(&mut raw_config).unwrap();
    serde_yaml::from_str(&raw_config).unwrap()
}

fn main() {
    let config = load_config("config.yaml");
    let devpi_client = devpi::client::DevpiClient::new(config.url, config.auth_token);
    let path = env::args().nth(1).unwrap();
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);
    for line in file.lines() {
        let l = line.unwrap();
        let spec = parser::parse_requirement_specifier(l);
        let versions = devpi_client.get_versions(&spec.name);
        if let Some((local_version, latest_devpi_version)) = check_version(&spec, &versions) {
            println!("{} is out of date: local version={}, devpi version={}",
                     spec.name,
                     local_version,
                     latest_devpi_version);
        }
    }
}
